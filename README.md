# Packages

A repo for building packages using my tiny build system.

Just rename the `.gitlab-ci.yml.templ` file to `.gitlab-ci.yml` and update it to your configuration.
Do that on a different branch with the name of the package you are building.
That way only the programs that are being updated are being build, not all of the programs in this repo.

All ci configs are under licenses on their respective branches.
This one has no license and is free to use without restrictions.
